<?php

namespace Drupal\blocce\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\blocce\BlocceExport;

class ExportBulkForm extends FormBase {
  public function buildForm(array $form, FormStateInterface $form_state) {
    $options = array();
    $query = Database::getConnection()->select('block_content_field_data','bcfd');
    $query->fields('bcfd',['id','info']);
    $blocks = $query->execute();
    foreach ($blocks as $single) {
      $options[$single->id]['id'] = [
        '#plain_text' => $single->id,
      ];
      $options[$single->id]['name'] = [
        '#plain_text' => $single->info,
      ];
    }

    $form['yaml_export'] = array(
      '#type' => 'textarea',
      '#attributes' => array(
        'style' => 'display: none',
      ),
    );
    if (!empty($_SESSION['blocce_yaml_export'])) {
      $form['yaml_export']['#default_value'] = $_SESSION['blocce_yaml_export'];
      $form['yaml_export']['#title'] = t('Select the YAML here:');
      unset($form['yaml_export']['#attributes']['style']);
      unset($_SESSION['blocce_yaml_export']);
    }
    $form['export_mode'] = array(
      '#type' => 'select',
      '#title' => t('Select the method you would like to use to export your block content.'),
      '#options' => array(
        'ui' => t('User Interface Export'),
        'fs' => t('Filesystem Export'),
      ),
      '#default_value' => \Drupal::config('blocce.settings')->get('export_dest'),
    );
    $thead = array(
      'id' => t('ID'),
      'name' => t('Block name'),
    );
    $form['table'] = array(
      '#type' => 'tableselect',
      '#header' => $thead,
      '#options' => $options,
      '#attributes' => array(
        'id' => 'blocce_blocks',
      ),
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Export selected blocks'),
      '#button_type' => 'primary',
    );
    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $exp = $form_state->getValue('table');
    $pre = BlocceExport::getPreExport(-1, $exp);
    $yaml = BlocceExport::buildYaml($pre);
    BlocceExport::saveYaml($form_state->getValue('export_mode'), $yaml);
  }
  
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // No form validation required
  }

  public function getFormId() {
    return "blocce_export_bulk";
  }
}