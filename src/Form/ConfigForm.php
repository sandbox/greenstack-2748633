<?php

namespace Drupal\Blocce\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;

/**
 * Provides the UI to handle general Blocce configuration.
 */
class ConfigForm extends FormBase {
  
   /**
   * {@inheritdoc}.
   */ 
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::config('blocce.settings');
    $form['head'] = array(
      '#type' => 'markup',
      '#markup' => '<h2>Blocce Configuration Settings</h2>',
    );
    $form['export_dest'] = array(
      '#type' => 'select',
      '#title' => t('Configure your default export settings.'),
      '#options' => array(
        'ui' => t('User Interface Export'),
        'fs' => t('Filesystem Export'),
       ),
      '#default_value' => $config->get('export_dest'),
    );
    $form['export_list'] = array(
      '#type' => 'select',
      '#title' => t('Select default listing mode for export.'),
      '#options' => array(
        'black' => t('Blacklist'),
        'white' => t('Whitelist'),
        'none' => t('Total'),
      ),
      '#default_value' => $config->get('export_list'),
    );
    $form['import_from'] = array(
      '#type' => 'select',
      '#title' => t('Configure your default import settings.'),
      '#options' => array(
        'ui' => t('User Interface Import'),
        'fs' => t('Filesystem Import'),
      ),
      '#default_value' => $config->get('import_from'),
    );
    $form['show_bl'] = array(
      '#type' => 'checkbox',
      '#title' => t('Display blacklisted blocks when importing.'),
      '#default_value' => $config->get('show_bl'),
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#name' => 'save',
      '#value' => t('Save configuration'),
      '#button_type' => 'primary'
    );
    $form['cancel'] = array(
      '#type' => 'submit',
      '#name' => 'cancel',
      '#value' => t('Cancel'),
    );
    $form['reset'] = array(
      '#type' => 'submit',
      '#name' => 'reset',
      '#value' => t('Reset'),
    );
    return $form;
  }

  /**
   * {@inheritdoc}.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $btn_name = $form_state->getTriggeringElement()['#name'];
    drupal_set_message($btn_name);
    $config = \Drupal::service('config.factory')->getEditable('blocce.settings');
    if ($btn_name == 'reset') {
      $config->set('export_list','black');
      $config->set('export_dest','ui');
      $config->set('import_from','ui');
      $config->set('show_bl',1);
      $config->save();
      drupal_set_message(t('Blocce\'s settings have been reset.'));
      return;
    }
    else if ($btn_name == 'cancel') {
      return;
    }

    $config->set('export_list', $form_state->getValue('export_list'));
    $config->set('export_dest', $form_state->getValue('export_dest'));
    $config->set('import_from', $form_state->getValue('import_from'));
    $config->set('show_bl', $form_state->getValue('show_bl'));
    $config->save();

    drupal_set_message(t('Your configuration has been saved.'));
  }

  /**
   * {@inheritdoc}.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // No validation required!
  }

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return "blocce_config";
  }
}
