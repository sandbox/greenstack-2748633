<?php

namespace Drupal\blocce\Form;

use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;
use Drupal\blocce\BlocceImport;

/**
 * Provides the UI for importing the blocks.
 */
class ImportForm implements FormInterface {

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::config('blocce.settings');
    $blacklist = BlocceImport::getBlackListed();
    if (!is_null($blacklist) && ($config->get('show_bl'))){
      drupal_set_message(t('@l',['@l' => $blacklist]),'warning');
    }

    $form['head'] = array(
      '#type' => 'markup',
      '#markup' => t('<h1>Import block content</h1>'),
    );
    $form['import_help'] = array(
      '#type' => 'markup',
      '#markup' => t('<em>Put help text her?E</em>'),
    );
    $form['import_mode'] = array(
      '#type' => 'radios',
      '#title' => t('Select input mode'),
      '#default_value' => 'ui',
      '#options' => array(
        'ui' => t('User Interface'),
        'fs' => t('File system'),
      ),
      '#default_value' => $config->get('import_from'),
    );
    $form['ui_import'] = array(
      '#type' => 'textarea',
      '#title' => t('Paste the YAML code here.'),
    );
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Run import'),
      '#button_type' => 'primary',
    );

    return $form;
  }

  /**
   * {@inheritdoc}.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Now we get to convert the YAML into a SQL query. This is going to be
    // the hard part.
    $src = "";
    if ($form_state->getValue('import_mode') == 'ui') {
      $src = $form_state->getValue('ui_import');
    }
    else {
      $src = get_sync_dir() . 'blocce.blocks.yml';
    }
    $imported = Yaml::parse($src);
    $conn = Database::getConnection();
    $count = 0;
    foreach ($imported as $uuid => $block) {
      $upsertBcfd = $conn->upsert('block_content_field_data');
      $bid = BlocceImport::getBlockId($uuid);
      if (BlocceImport::isBlacklisted($bid)) {
        continue;
      }
      BlocceImport::upsertBco($uuid, $block);
      if (BlocceImport::bcbRowExists($bid)) {
        $upsertBcb = $conn->upsert('block_content__body');
        $bcbFields = [
          'entity_id' => $bid,
          'revision_id' => $bid,
          'langcode' => $block['langcode'],
          'bundle' => $block['bundle_type'],
          'deleted' => $block['bcb']['deleted'],
          'delta' => $block['bcb']['delta'],
          'body_value' => $block['bcb']['body_value'],
          'body_summary' => $block['bcb']['body_summary'],
          'body_format' => $block['bcb']['body_format'],
        ];
        $upsertBcb->fields($bcbFields);
        $upsertBcb->key('entity_id');
        $upsertBcb->execute();
      }

      $bcfdFields = [
        'id' => $bid,
        'revision_id' => $bid,
        'langcode' => $block['langcode'],
        'type' => $block['bundle_type'],
        'info' => $block['bcfd']['info'],
        'changed' => $block['bcfd']['changed'],
        'revision_translation_affected' => $block['bcfd']['revision_translation_affected'],
        'default_langcode' => $block['bcfd']['default_langcode'],
      ];
      $upsertBcfd->fields($bcfdFields);
      $upsertBcfd->key('id');
      $upsertBcfd->execute();
      ++$count;
    }
    // This current method will cause issues.
    drupal_set_message(t("Your configuration has been updated."));
    \Drupal::logger('blocce')->notice('Successfully configured ' . $count . ' blocks.');
  }

  /**
   * {@inheritdoc}.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('import_mode') == 'fs') {
      if (!BlocceImport::importFileExists()) {
        $form_state->setErrorByName('import_mode', 'Please review the errors.');
      }
      return;
    }

    // If UI import, then make sure that the input is valid YAML.
    $errMessage = [];
    try {
      $imported = Yaml::parse($form_state->getValue('ui_import'), TRUE);
      // I'm tempted to name $key $foo but I won't
      // $bar == block array.
      $elementsPerBlock = 13;
      foreach ($imported as $key => $bar) {
        if (count($bar, COUNT_RECURSIVE) != $elementsPerBlock) {
          array_push($errMessage, 'Your YAML is either missing some elements or has extras. Please ensure that you copied the code correctly.');
          break;
        }
      }
    }
    catch (ParseException $e) {
      array_push($errMessage, 'The supplied YAML is invalid on line <em>' . $e->getParsedLine() . '</em> around <em>' . $e->getSnippet() . '</em>');
    }
    $errCount = count($errMessage);
    if ($errCount > 0) {
      // There might be other errors in the future that I'm not aware of.
      $message = ($errCount > 1) ? implode(' ', $errMessage) : $errMessage[0];
      $form_state->setErrorByName('ui_import', t('@message',['@message' => $message]));
    }
  }

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return "blocce_import_form";
  }

}
