<?php

namespace Drupal\blocce\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\blocce\BlocceConfigStorage;

/**
 * Provides basic functionality for saving configuration for Blocce.
 */
class BlocceConfigForm extends FormBase {

  /**
   * Constructs the form.
   *
   * {@inheritdoc}.
   *
   * @return form
   *   The BlocceConfigForm.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    
    // Basic navigation and info.
    $form['links'] = array(
      '#type' => 'markup',
      '#markup' => "Configure general behavior of the Blocce module here.",
    );

    // Table Header.
    $thead = array(
      'id' => t('ID'),
      'name' => t('Block name'),
      'lmode' => t('Select listing mode'),
      'ilmode' => t('Select import listing'),
    );

    $form['table'] = array(
      '#type' => 'table',
      '#header' => $thead,
      '#attributes' => array(
        'id' => 'blocce_blocks',
      ),
      '#attached' => array(
        'library' => array(
          'blocce/settings',
        ),
      ),
    );

    foreach (BlocceConfigStorage::getBlocks() as $block) {
      // I really wish there were a better way to do this.
      if ($block->listing == NULL || $block->listing == 0) {
        // Set select to unlisted.
        $default = 'none';
      }
      elseif ($block->listing == 1) {
        // Set select to blacklist.
        $default = 'black';
      }
      elseif ($block->listing == 2) {
        // Set select to whitelist.
        $default = 'white';
      }
      else {
        $arr = [
          '%block' => $block->info,
          '%listing' => $block->listing,
        ];
        \Drupal::logger('blocce')
          ->error(t('%block has an invalid listing value of %listing in the database.'), $arr);
        throw new Exception("Error on getting export listing value. See the log.");
      }

      if ($block->im_list == NULL || $block->im_list == 0) {
        //Set select to unlisted.
        $im_default = 'none';
      }
      elseif ($block->im_list == '1'){
        $im_default = 'black';
      }
      else {
        $arr = [
          '%block' => $block->info,
          '%listing' => $block->im_list,
        ];
        \Drupal::logger('blocce')
          ->error(t('%block has an invalid import listing value of %listing in the database.'), $arr);
        throw new Exception("Error on getting import listing value. See the log.");
      }

      $form['table'][$block->id]['id'] = [
        '#plain_text' => $block->id,
      ];
      $form['table'][$block->id]['name'] = [
        '#plain_text' => $block->info,
      ];
      $form['table'][$block->id]['listing'] = [
        '#type' => 'select',
        '#options' => [
          'none' => t('Unlisted'),
          'black' => t('Blacklist'),
          'white' => t('Whitelist'),
        ],
        '#attributes' => [
          'id' => 'select-block-' . $block->id,
        ],
        '#default_value' => $default,
      ];
      $form['table'][$block->id]['im_listing'] = [
        '#type' => 'select',
        '#options' => [
          'none' => t('Unlisted'),
          'black' => t('Blacklist'),
        ],
        '#attributes' => [
          'id' => 'select-block-import-' . $block->id,
        ],
        '#default_value' => $im_default,
      ];
    }

    $form['submit'] = array(
      '#type' => 'submit',
      '#name' => 'save',
      '#value' => t('Save configuration'),
      '#button_type' => 'primary',
    );
    $form['cancel'] = array(
      '#type' => 'submit',
      '#name' => 'abort',
      '#value' => t('Cancel'),
      '#button_type' => 'cancel',
    );
    $form['reset'] = array(
      '#type' => 'submit',
      '#name' => 'reset',
      '#value' => t('Reset configuration'),
    );
    $form['json-container'] = array(
      '#type' => 'textarea',
      '#attributes' => array(
        'id' => 'json-container',
        'style' => 'visibility: hidden;',
      ),
    );
    return $form;
  }

  /**
   * Runs the database query to save the configuration.
   *
   * {@inheritdoc}.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // The cancel button has been pressed; we don't want to save any edits.
    $btn_name = $form_state->getTriggeringElement()['#name'];
    if ($btn_name == 'reset') {
      $form_state->setRedirect('blocce.reset');
      return;
    }
    else if ($btn_name != 'save') {
      return;
    }
    $json = $form_state->getValue('json-container');
    $choices = json_decode($json);
    // Contains the blocks with their listing changed.
    $changed = array();
    foreach ($choices as $id => $subarr) {
      // Check if the block has been changed.
      if ($subarr[0] != $subarr[1]) {
        // Add to an array with the new option.
        $changed[$id] = $subarr[1];
      }
    }
    BlocceConfigStorage::storeConfig($changed);
  }

  /**
   * {@inheritdoc}.
   *
   * @return string
   *   The form's id.
   */
  public function getFormId() {
    return "blocce_config_form";
  }

  /**
   * {@inheritdoc}.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // We don't need no validification in this from.
  }

}
