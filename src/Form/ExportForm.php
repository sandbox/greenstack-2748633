<?php

namespace Drupal\blocce\Form;

use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\blocce\BlocceExport;

/**
 * Provides the form to export the blocks according to the settings.
 */
class ExportForm implements FormInterface {

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::config('blocce.settings');
    $form['head'] = array(
      '#type' => 'markup',
      '#markup' => t('<h1>Export block contents</h1>'),
    );
    $form['yaml_export'] = array(
      '#type' => 'textarea',
      '#attributes' => array(
        'style' => 'display: none',
      ),
    );
    // Unless it exists, we don't need to create this element.
    // get this to show up if the user presses the button.
    if (!empty($_SESSION['blocce_yaml_export'])) {
      $form['yaml_export']['#default_value'] = $_SESSION['blocce_yaml_export'];
      $form['yaml_export']['#title'] = t('Select the YAML here:');
      unset($form['yaml_export']['#attributes']['style']);
      unset($_SESSION['blocce_yaml_export']);
    }
    $form['export_mode'] = array(
      '#type' => 'select',
      '#title' => t('Select the method you would like to use to export your block content.'),
      '#options' => array(
        'ui' => t('User Interface Export'),
        'fs' => t('Filesystem Export'),
      ),
      '#default_value' => $config->get('export_dest'),
    );
    $form['export_help'] = array(
      '#type' => 'markup',
      '#markup' => t('<em>User Interface Export</em> gives you YAML code to copy
			 and paste to the new site.<br /><em>Filesystem Export</em> will generate
			  a YAML file in your system that can be tracked by git.'),
    );
    $form['listing_mode'] = array(
      '#type' => 'select',
      '#title' => t('Select listing mode for export.'),
      '#options' => array(
        'black' => t('Blacklist'),
        'white' => t('Whitelist'),
                // 'once'	=> t('One-time'),.
        'none'     => t('Total'),
      ),
      '#default_value' => $config->get('export_list'),
    );
    $form['listing_help'] = array(
      '#type' => 'markup',
      '#markup' => t('<em>Blacklist</em>: All blocks except for the ones on the 
					blacklist will be exported.<br />
                    <em>Whitelist</em>: Only the blocks on the whitelist will be exported.<br />
                    <em>Total</em>: All blocks will be exported, regardless of their listing.<br />'),
    );
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Generate export'),
      '#button_type' => 'primary',
    );
    return $form;
  }

  /**
   * {@inheritdoc}.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Here we'll add the code that gets the block data from the database.
    $mode = $form_state->getValue('listing_mode');
    // Turn mode into something that the coming function understands.
    $mode = get_listing_val($mode);
    $res = BlocceExport::getPreExport($mode);
    $yml = BlocceExport::buildYaml($res);

    $dest = $form_state->getValue('export_mode');
    BlocceExport::saveYaml($dest, $yml);
  }

  /**
   * {@inheritdoc}.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Form doesn't require validation.
  }

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return "blocce_export_form";
  }

}
