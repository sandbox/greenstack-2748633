<?php

namespace Drupal\blocce\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\blocce\BlocceReset;

/**
 * Provides the UI for resetting all blocce configuration.
 */
class ResetForm extends FormBase {
  
  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['head'] = array(
      '#type' => 'markup',
      '#markup' => t('<h1>Select what you want to reset.</h1>'),
    );
    $form['selections'] = array(
      '#type' => 'checkboxes',
      '#options' => array(
        'export' => t('Export List'),
        'import' => t('Import List'),
      ),
    );
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Reset'),
      '#button_type' => 'primary',
    );

    return $form;
  }

  /**
   * {@inheritdoc}.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $checked = BlocceReset::selectedBoxes($form_state->getValue('selections'));
    $conn = Database::getConnection();
    $update = $conn->update('blocce_config');
    $fields = [];
    if ($checked['export']) {
      $fields['listing'] = '0';
    }
    if ($checked['import']) {
      $fields['im_list'] = '0';
    }
    $update->fields($fields);
    $update->execute();
    drupal_set_message('Your settings have been reset.');
    $form_state->setRedirect('blocce.settings');
  }

  /**
   * {@inheritdoc}.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (BlocceReset::boxesEmpty($form_state->getValue('selections'))) {
      $form_state->setErrorByName('selections',t('Please select at least one option.'));
    }
  }

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'blocce_reset_form';
  }
}
