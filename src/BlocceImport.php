<?php

namespace Drupal\blocce;

use Drupal\Core\Database\Database;

/**
 * Utility class for functions tied with importing block configuration.
 */
class BlocceImport {

  /**
   * Gets the id of the block from the database based on its uuid.
   *
   * @param int $uuid
   *   The uuid of the block whose internal db id you need to retrieve.
   *
   * @return int
   *   The id of the block or -1 if the block wasn't found.
   */
  public static function getBlockId($uuid) {

    $conn = Database::getConnection();
    $sel = $conn->select('block_content', 'bc');
    $sel->fields('bc', ['id']);
    $sel->condition('bc.uuid', $uuid);
    $id = $sel->execute()->fetch()->id;
    // Return -1 if block is not found; id otherwise.
    return (is_null($id) ? -1 : $id);
  }

  /**
   * Inserts or updates (i.e. upserts) the block_content table in the database.
   *
   * @param int $uuid
   *   The uuid of the block to update.
   * @param int $block
   *   The array containing the block info.
   */
  public static function upsertBco($uuid, $block) {

    $conn = Database::getConnection();
    $bid = self::getBlockId($uuid);
    $fields = [
      'langcode' => $block['langcode'],
      'type' => $block['bundle_type'],
    ];
    // The block isn't in the database.
    if ($bid == -1) {
      // Do an insert.
      $insert = $conn->insert('block_content');
      $fields['uuid'] = $uuid;
      $insert->fields($fields);
      // Block created!
      $insert->execute();
    }
    else {
      // Do an update.
      $update = $conn->update('block_content');
      $update->fields($fields);
      $update->condition('id', $bid);
      $update->execute();
    }
  }

  /**
   * Retrieves the yaml from the filePath.
   *
   * @see file_get_contents
   *
   * @return string|false
   *   The path of the sync file or false if it doesn't exist or isn't found.
   */
  public static function importFileExists() {

    $name = 'blocce.blocks.yml';
    $sdir = get_sync_dir();
    $fpath = $sdir . $name;
    if (!file_exists($sdir)) {
      \Drupal::logger('blocce')->error('Couldn\'t find the sync directory.');
      drupal_set_message('Couldn\'t find the sync directory.', 'error');
      return FALSE;
    }
    elseif (!file_exists($fpath)) {
      $vals = ['%name' => $name, '%dir' => $sdir];
      \Drupal::logger('blocce')->error('Couldn\'t find %name in %dir', $vals);
      drupal_set_message(
        t("The sync file couldn't be found. Please check the log."), 'error'
        );
      return FALSE;
    }

    return $fpath;
  }

  /**
   * Determines whether or not data has been made in block_content__body.
   *
   * @param int $id
   *   The block id to search for in the database.
   *
   * @return bool
   *   Whether or not the block was found within the database.
   */
  public static function bcbRowExists($id) {
    $conn = Database::getConnection();
    $sel = $conn->select('block_content__body', 'bcb');
    $sel->addField('bcb', 'langcode');
    $sel->condition('entity_id', $id);
    $res = $sel->execute()->fetchField();
    return !empty($res);
  }

  /**
   * Retrieves a list of blacklisted blocks for import.
   *
   * @return string|null
   *   A string representing the blocks that have been blacklisted for import.
   *   Will return null if there are none.
   */
  public static function getBlacklisted() {
    $query = Database::getConnection()->select('blocce_config','bc');
    $query->join('block_content_field_data','bcfd','bc.entity_id = bcfd.id');
    $query->fields('bcfd',['info']);
    $query->condition('bc.im_list',1); // Select the blacklisted blocks only
    $result = $query->execute();
    $list = array();
    foreach ($result as $blacklisted) {
      array_push($list, $blacklisted->info);
    }
    if (count($list) > 0){
      $str = implode(', ', $list);
      return "The following block(s) have been blacklisted: " . $str;
    }
    else {
      return NULL;
    }
  }

  /**
   * Retrieves the import blacklisting status of the block.
   *
   * @param int $bid
   *   The block id of the block to check if it's blacklisted.
   *
   * @return bool
   *   True if the block is blacklisted, otherwise false.
   */
  public static function isBlacklisted($bid) {
    $query = Database::getConnection()->select('blocce_config','bc');
    $query->addField('bc','im_list');
    $query->condition('bc.entity_id',$bid);
    $res = $query->execute()->fetchField();
    if (is_null($res) || $res == 0){
      return FALSE;
    }
    else {
      return TRUE;
    }
  }
}
