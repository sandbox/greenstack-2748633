<?php

namespace Drupal\blocce;

use Symfony\Component\Yaml\Yaml;
use Drupal\Core\Database\Database;
use Drupal\Core\Database\Statement;

/**
 * Provides extra utility for the BlocceExportForm.
 */
class BlocceExport {

  /**
   * Retrieves the information regarding the blocks in the database.
   *
   * @param int $listmode
   *   The listing mode with which to filter the blocks by.
   * @param mixed[] $array
   *   
   * @return StatementInterface
   *   The result of the executed query.
   */
  public static function getPreExport($listmode, $array = []) {
    $db = Database::getConnection();
    $query = $db->select('blocce_config', 'bc');
    $query->rightJoin('block_content', 'bco', 'bco.id = bc.entity_id');
    $query->leftJoin('block_content__body', 'bcb', 'bco.id = bcb.entity_id');
    $query->join('block_content_field_data', 'bcfd', 'bcfd.id = bco.id');
    $query->fields('bc', ['id', 'listing'])
            ->fields('bco')
            ->fields('bcb',
              ['bundle',
                'deleted',
                'body_value',
                'body_summary',
                'body_format',
                'delta',
              ])
            ->fields('bcfd',
              ['info',
                'changed',
                'revision_translation_affected',
                'default_langcode',
              ]);
    if ($listmode == 1) {
      $query->where('bc.listing != 1 OR bc.listing IS NULL');
    }
    elseif ($listmode == 2) {
      $query->condition('bc.listing', '2');
    }
    elseif ($listmode == -1) {
      $query->condition('bcfd.id', $array, 'in');
    }
    // Actually there's no condition here. 'None' in Export means
    // 'ignore listing'.
    return $query->execute();
  }

  /**
   * Generates YAML from the database query results from getPreExport.
   *
   * @param Statement $qResult
   *   the query results obtained from getPreExport.
   *
   * @return string
   *   A yaml string produced by Drupal's Yaml::dump
   *
   * @see getPreExport()
   */
  public static function buildYaml(Statement $qResult) {
    $result = [];
    foreach ($qResult as $block) {
      $result[$block->uuid] = [
        'langcode' => $block->langcode,
        'bundle_type' => $block->type,
        'bcb' => [
          'deleted' => $block->deleted,
          'delta' => $block->delta,
          'body_value' => $block->body_value,
          'body_summary' => $block->body_summary,
          'body_format' => $block->body_format,
        ],
        'bcfd' => [
          'info' => $block->info,
          'changed' => $block->changed,
          'revision_translation_affected' => $block->revision_translation_affected,
          'default_langcode' => $block->default_langcode,
        ],
      ];
    }

    // This is so we can make sure that everything is formatted as it should.
    $yaml_layers = 3;
    return Yaml::dump($result, $yaml_layers);
  }

  public static function saveYaml($dest, $yml) {
    if ($dest == 'ui') {
      $_SESSION['blocce_yaml_export'] = $yml;
      // The user will see the YAML at the top of the page after this.
      drupal_set_message(t('Your settings have been exported. Please copy
      the YML code below and either paste it on the site you wish to 
      import these settings to or save it for later use.'));
    }
    else {
      // Store the yaml file into the filesystem sync directory.
      $sdir = get_sync_dir();
      // Current directory.
      $path = "blocce.blocks.yml";
      if (file_exists($sdir)) {
        $fpath = $sdir . $path;
        file_put_contents($fpath, $yml);
        drupal_set_message(
          t("Your export file has been saved in %fpath", ['%fpath' => $fpath]
          ));
      }
      else {
        \Drupal::logger('blocce')->error('Couldn\'t find the sync directory.');
      }
    }
  }
}
