<?php

namespace Drupal\blocce;

/**
 * Provides some functionality for resetting blocce configuration.
 */
class BlocceReset {
  
  /**
   * Determines if all checkboxes are empty.
   *
   * @param mixed[] $array
   *   The array provided when retrieving the checkboxes by getValue().
   *
   * @return bool
   *   TRUE if the array is empty or no elements are selected; FALSE otherwise.
   */
  public static function boxesEmpty($array) {
    foreach ($array as $value) {
    	if ($value != '0') {
    		return FALSE;
    	}
    }
    return TRUE; // array is either empty or has no selected elements
  }

  /**
   * Gets an array representing of the selected checkboxes.
   *
   * @param mixed[] $array
   *   The array provided when retrieving the checkboxes by getValue().
   *
   * @return bool[]
   *   An associative array keyed by element name with a bool value for select.
   */
  public function selectedBoxes($array) {
    $checked = [];
    foreach ($array as $box => $value) {
      $checked[$box] = $value != '0';
    }
    return $checked;
  }
}
