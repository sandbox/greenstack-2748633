<?php

namespace Drupal\blocce;

use Drupal\Core\Database\Database;

/**
 * Provides utility functions for the config form.
 */
class BlocceConfigStorage {

  /**
   * Gets all the blocks and their names.
   *
   * Perhaps most importantly, it grabs
   * the ids of the blocks, which is going to be important for us to remember
   * which ones we've blacklisted for export and which ones were whitelisted.
   */
  public static function getBlocks() {
    $db = Database::getConnection();
    $query = $db->select('block_content_field_data', 'bcfd');
    // we'll do a null check for a default value.
    $query->leftJoin('blocce_config', 'bc', 'bcfd.id = bc.entity_id');
    $query->fields('bcfd', ['id', 'info'])
      ->fields('bc', ['entity_id', 'listing', 'im_list'])
      ->orderBy('id', 'ASC');
    $result = $query->execute();
    return $result;
  }

  /**
   * Uploads the configuration settings to the database.
   *
   * @param mixed[] $blocks
   *   An array of block ids that are being configured.
   */
  public static function storeConfig($blocks) {
    $db = Database::getConnection();
    foreach ($blocks as $id => $listing) {
      $upsert = $db->upsert('blocce_config');
      $listing_key = (strpos($id, 'im') ? 'im_list' : 'listing');
      $upsert->fields([
        'entity_id' => extrapolate_bid($id),
        $listing_key => get_listing_val($listing),
      ]);
      $upsert->key('entity_id');
      $upsert->execute();
    } 
    drupal_set_message("Your configuration has been saved.");
  }

}
