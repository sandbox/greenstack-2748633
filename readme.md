CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
   * Blocks
   * General Settings
 * Using the Module
   * Exporting
   * Importing
 * Design Decisions
 * Planned Features


INTRODUCTION
------------

Blocce, which is short for "Block Content Exporter" can be pronounced either as
"blotchy" or "bloxy"; either way works. However, since "bloxy" sounds more like
"blocks" and less like a disease, you may prefer that. I, Greenstack, like them
both, and depending on the mood I'm in, that will change.

The purpose of this module is to facilitate moving block contents from one site
to another, say from dev to staging to live.

INSTALLATION
------------

Installing Blocce is the same as installing any other module - put it in your
site's modules folder, go to the extend page, and turn on the module.

All that Blocce really requires is that the core block module be installed, as
this module depends on blocks in order to function properly.

For a good summary about what Blocce is about, please see the project page at
https://www.drupal.org/sandbox/greenstack/2748633

Note: If you're updating from 8.x-1.x to 8.x-1.0-alpha1+, you will need to
uninstall and then reinstall the module in order to make use of the built-in
configuration.


CONFIGURATION: BLOCKS
---------------------

Blocce allows you to select which blocks can be exported as well as which ones
should be excluded, as well as blacklisting any blocks that you don't want to
be overridden by an import. To configure these settings for each block, go to
admin/config/blocce. A list of all the blocks you have on the site will be
displayed, showing their block id, name, as well as their current listing
status for exporting and importing.

The options available for export listing are as follows:
  Unlisted: These blocks will be included in a blacklist export, but excluded
    in a whitelist export.
  Blacklist: Blocks set to blacklist will be excluded in a blacklist export and
    whitelist export, but will still be included in a total export.
  Whitelist: Blocks set to whitelist will be exported in every export. This can
    be useful when trying to limit the amount of blocks you want to export.

The options available for import listing are:
  Unlisted: These blocks will be overridden if the block is included in the
    import file or text.
  Blacklist: These blocks will never be overridden under any circumstance while
    they are still in this state.

When you visit the import page, all your blacklisted blocks will be listed at
the top. There is currently no functionality to suppress this notification.

The reason I decided not to include a whitelist for import is due to the large
overlap with the unlisted option. If you don't want a block to be overridden on
a single import, you'll need to temporarily set it to the blacklist, and unlist
it whenever you're ready for it to be updated.

If you wish to reset the listing status for all blocks to unlisted, access the
reset page from admin/config/blocce by clicking "Reset configuration" at the
bottom of the page, where you can choose to either reset the Export Listings or
the Import Listings. This operation cannot be undone.


CONFIGURATION: GENERAL
----------------------

Blocce currently has four options to set in the general settings section, which
is reached by navigating to admin/config/blocce/settings, or by clicking on the
"General settings" subtab in the Settings tab.

The settings here will affect what's selected by default when performing either
an import or an export. You may set the default import retrieval section, the
default export saving location (see the Exporting Blocks section), the kind of
export you wish to perform by default, and whether or not you want to see the
blocks you've blacklisted for import on the respective form.

Resetting the module's settings is as simple as hitting the reset button.


USING THE MODULE
----------------

You'll interact with the module entirely in the admin/config/blocce area. The
self-explanatory tabs are what will give you the functionality that you need.


EXPORTING BLOCKS
----------------

When exporting your block content, you have two options: you can either do a UI
export or a filesystem export. A User Interface Export will give you text to
copy from your dev site and past in the Import section of your live site. On
the other hand, a Filesystem Export will create a .yml file in your sync
directory that can be managed via git.

Additionally, you have the option to select which kind of import you wish to
perform. You have three options:
  1. Blacklist export: All blocks will be exported, except for those that have
     been put on the blacklist.
  2. Whitelist export: Only blocks marked as whitelisted will be exported.
  3. Total export: All blocks, regardless of listing value, will be exported.

Finally, there's the bulk export. Similar to the Standard Export mode, you have
the ability to be able to send the export YAML to either the UI or to the sync
directory; however, you select individual blocks to be exported. Though there
is some overlap with the Total export, this is useful for a variety of
circumstances, such as needing to export only one block, a mix of whitelisted
and blacklisted blocks, a few blocks in each group, and other similar
situations.

IMPORTING BLOCKS
----------------

Like exporting, you have a filesystem and user interface imports.

To perform a user interface import, copy the ui export YAML provided when you
or your coworker did the export. Paste that yaml into the field and click on
run import.

To do the filesystem import, please ensure that you have the blocce.blocks.yml
saved in your sync folder, as that is where the module will look for the file.
After selecting the file system option, click "run import" just like before and
the import should go off without a hitch.

Also, to reitirate, the notification at top is just to remind you which blocks
have been blacklisted, preventing them from being overridden.


DESIGN DECISIONS
----------------

I decided to included blocks that have been 'deleted' within Drupal itself that
are still in the database so as to be able to transfer this to the other sites.
Once the block has been deleted on the other site, it may be a good idea to
blacklist that block for export and/or import so that it doesn't take up any
more space on the disk (if you choose to do a filesystem import/export route)
and makes the selection smaller for User Interface importing and exporting.

In the future, this might be changed so that deleted blocks are put into a
separate array to tell Drupal to delete those blocks, but this is currently low
priority.


PLANNED FEATURES
----------------

I'm currently not planning any new features, as I'm preparing this module for a
beta release. Therefore, 8.x-1.0 is on feature freeze.
