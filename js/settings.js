(function($){

var omap = {};
//These are just to help me remember which indexes
const C_OR = 0;
const C_NEW = 1;
//let's see if this works
var json;

//Get eveything to listen for a change
$(document).ready(function(){
	$('#blocce_blocks').find('tbody').find('select').each(function(){
		omap[$(this).attr('id')] = [$(this).val(), $(this).val()];
		//give the select an on change event
		$(this).on('change', function(e){e.preventDefault();compare(e)});
	});
	//prepare to send the json
	$('#edit-submit').on('click',function(){
		$('#json-container').val(JSON.stringify(omap));
	});
});

// Highlights any row that has a different selection than what was originally
// selected. Also stores the new selection in the same 'map'.
function compare(e){
	var choice = $(e.target).val();
	var elid = $(e.target).attr('id');
	if (omap[elid][C_OR] != choice){
		omap[elid][C_NEW] = choice;
		//highlight the parent row
		$(e.target).parents('tr').css("background-color", "#D0F5D3");
	}
	else {
		omap[elid][C_NEW] = omap[elid][C_NEW]; 
		//remove the highlight on the parent row
		$(e.target).parents('tr').removeAttr('style');
	}
}

})(jQuery);